import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import { Font } from 'expo';

import SignInScreen from './screens/Auth/SignIn.js'
import AuthLoadingScreen from './screens/Auth/AuthLoading.js'
import HomeScreen from './screens/App/Home.js'



const AppStack = createStackNavigator({
  Home: HomeScreen,
}, {
  // initialRouteName: 'Home', // DEV
  headerMode: 'none'
} )
const AuthStack = createStackNavigator({ SignIn: SignInScreen }, {headerMode: 'none'})
const Nav = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none'
  }
)

export default class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {assetsLoaded: false}
  }

  async componentDidMount() {
    await Font.loadAsync({
      'Nunito': require('./assets/fonts/Nunito-Regular.ttf'),
      'Nunito-SemiBold': require('./assets/fonts/Nunito-SemiBold.ttf'),
      'Nunito-ExtraBold': require('./assets/fonts/Nunito-ExtraBold.ttf'),
      'Icons': require('./assets/fonts/icons/icomoon.ttf'),
    });
    this.setState({assetsLoaded: true})
  }

  render() {
    if(!this.state.assetsLoaded) return false
    return (
      <Nav/>
    )
  }
}