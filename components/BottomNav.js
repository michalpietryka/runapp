import React from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Icon from '../components/Icon.js'
import styles from '../styles.js'

const circles_size = 90
const round_size = 68

export default class BottomNav extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let round_offset = (circles_size - round_size) / 2

        return (
            <View style={styles.BottomNavContainer}>
                <TouchableOpacity onPress={this.props.createDummyRace} style={{width: '33.33%', alignItems: 'center'}}>
                    <Icon style={StyleSheet.flatten([styles.clPrimary, this.props.style, {fontSize: 24}])} icon={'leaderboard_outline'}></Icon>
                    <Text style={StyleSheet.flatten([styles.clBlack, styles.fontSemiBold, {fontSize:8, marginBottom: 24}])}>Leaderboard</Text>
                </TouchableOpacity>

                <View style={{width: '33.33%', alignItems: 'center'}}>
                    <View style={{position: 'absolute', bottom: 20, zIndex: 200}}>
                        <Image style={{width: circles_size, height: circles_size}} source={require('../assets/img/circles.png')}></Image>
                        <TouchableOpacity onPress={this.props.openContent} style={{width: round_size, height: round_size, textAlign: 'center', borderRadius: round_size, backgroundColor: '#ffffff', elevation: 5, position:'absolute', bottom: round_offset, left: round_offset}}>
                            <Icon style={StyleSheet.flatten([styles.clPrimary, this.props.style, {fontSize: 32, marginTop: 18}])} icon={'sneaker'}></Icon>
                        </TouchableOpacity>
                    </View>
                    <Text style={StyleSheet.flatten([styles.clBlack, styles.fontSemiBold, {fontSize:8, marginTop: 24}])}>Competitions+</Text>
                </View>

                <TouchableOpacity onPress={this.props.dummyButton} style={{width: '33.33%', alignItems: 'center'}}>
                    <Icon style={StyleSheet.flatten([styles.clPrimary, this.props.style, {fontSize: 24}])} icon={'runners_outline'}></Icon>
                    <Text style={StyleSheet.flatten([styles.clBlack, styles.fontSemiBold, {fontSize:8, marginBottom: 24}])}>Runners</Text>
                </TouchableOpacity>
            </View>
        )
    }
}