import React from 'react';
import {
    ImageBackground,
    StyleSheet,
    ScrollView,
    View,
    Image,
} from 'react-native';

export default class Container extends React.Component {
    constructor(props) {
        super(props)

        this.renderLoading = this.renderLoading.bind(this)
    }

    renderLoading() {
        if(this.props.isLoading) {
            return (
                <View style={{
                    position:'absolute',
                    backgroundColor: 'rgba(0,0,0,0.5)',
                    width: 800,
                    height: 1280,
                    left: 0,
                    top: 0,
                    zIndex: 900,
                    overflow: 'visible',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>

                    <Image style={{width: 40, height: 40}} source={require('../assets/img/spinner.gif')}></Image>
                </View>
            )
        }
    }

    render() {
        return (      
            <View style={{backgroundColor:'#ffffff', position:'relative', height: '100%'}}> 
                {this.renderLoading()}
                {this.props.children}   
            </View>
        )
    }
}