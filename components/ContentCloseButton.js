import React from 'react'
import {
    Text,
    TouchableOpacity,
    StyleSheet 
} from 'react-native';
import Icon from './Icon.js'

export default class ContentCloseButton extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity style={styles.ContentCloseButton} onLongPress={this.props.onLongPress} onPress={this.props.onPress}>
                {/* <Icon style={StyleSheet.flatten([this.props.style, {fontSize: 40, color: '#333333'}])} icon={this.props.icon}></Icon> */}
            </TouchableOpacity>
        )
    }
}