import React from 'react';
import {
    Text,
    StyleSheet
} from 'react-native';
import styles from '../styles.js'

const icon_expand = 59648
const icon_runners_outline = 59657
const icon_leaderboard_outline = 59661
const icon_sneaker = 59660
const icon_maximalize = 59655
const icon_minimalize = 59656
const icon_runners = 59658
const icon_dotline = 59653
const icon_you = 59659
const icon_close = 59651
const icon_bullet = 59650
const icon_flag = 59654

const icon_cog = 0 // do ustalenia
const icon_leaderboard = 0 // do ustalenia

export default class Icon extends React.Component {
    constructor(props) {
        super(props)
    }

    _getIcon(icon) {
        switch(icon) {
            case 'runners_outline': return String.fromCharCode(icon_runners_outline); break;
            case 'runners': return String.fromCharCode(icon_runners); break;
            case 'bullet': return String.fromCharCode(icon_bullet); break;
            case 'close': return String.fromCharCode(icon_close); break;
            case 'cog': return String.fromCharCode(icon_cog); break;
            case 'dotline': return String.fromCharCode(icon_dotline); break;
            case 'expand': return String.fromCharCode(icon_expand); break;
            case 'flag': return String.fromCharCode(icon_flag); break;
            case 'leaderboard': return String.fromCharCode(icon_leaderboard); break;
            case 'leaderboard_outline': return String.fromCharCode(icon_leaderboard_outline); break;
            case 'maximalize': return String.fromCharCode(icon_maximalize); break;
            case 'minimalize': return String.fromCharCode(icon_minimalize); break;
            case 'sneaker': return String.fromCharCode(icon_sneaker); break;
            case 'you': return String.fromCharCode(icon_you); break;
            default: return ''; break;
        }
    }

    render() {
        return (
            <React.Fragment>
                <Text style={StyleSheet.flatten([styles.Icon, this.props.style])}>{this._getIcon(this.props.icon)}</Text>
            </React.Fragment>
        )
    }
}