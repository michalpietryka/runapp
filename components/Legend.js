import React from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    Animated,
    StyleSheet
} from 'react-native';
import Icon from '../components/Icon.js'
import styles from '../styles.js'

const legend_width = 120

class IconContainer extends React.Component {
    render() {
        return (
            <View style={styles.LegendIconContainer}>
                <Icon style={StyleSheet.flatten([styles.clPrimary, this.props.style, {marginBottom: -16}])} icon={this.props.icon}></Icon>
                <Icon style={StyleSheet.flatten([styles.clGrey, this.props.style, {marginBottom: -8, fontSize: 32}])} icon={'dotline'}></Icon>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={StyleSheet.flatten([styles.clPrimary, styles.fontSemiBold, {fontSize:8, marginBottom: 24}])}>{this.props.value} </Text><Text style={StyleSheet.flatten([styles.clBlack, {fontSize:8}])}>{this.props.title}</Text>
                </View>
            </View>
        )
    }
}

export default class Legend extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isVisible: true,
            isOpen: false,

            slideLeft: new Animated.Value(-legend_width)
        }

        this._animateSlideLeft = this._animateSlideLeft.bind(this)
    }

    _animateSlideLeft() {
        let open = this.state.isOpen
        this.setState({
            isOpen: !open
        })

        Animated.timing(
            this.state.slideLeft,
            {
                toValue: !open ? 0 : -legend_width,                  
                duration: 500, 
            }
        ).start()
    }

    render() {
        if(!this.state.isVisible) return null 

        return (
            <Animated.View style={StyleSheet.flatten([styles.LegendContainer, {
                translateX: this.state.slideLeft
            }])}>
                <View style={StyleSheet.flatten([styles.LegendOffcanvas, {
                    width: legend_width,
                    elevation: 50
                }])}>
                    <IconContainer style={{fontSize: 32}} icon={'bullet'} value={5} title={'runners nearby'} />
                    <IconContainer style={{fontSize: 32}} icon={'you'} title={'Your position'} />
                    <IconContainer style={{fontSize: 32}} icon={'flag'} title={'Join competition'} />
                </View>

                <View>
                    <TouchableOpacity 
                        style={StyleSheet.flatten([styles.ToggleLegendButton, {
                            elevation: 10
                        }])} 
                        activeOpacity={1} 
                        onPress={this._animateSlideLeft}>
                        <Icon style={StyleSheet.flatten([styles.clPrimary, {fontSize: 18, transform: [{rotate: !this.state.isOpen ? '180deg' : '0deg'}]}])} icon={'expand'}></Icon>
                        {/* <Image source={require('../assets/img/btn_toggle_legend.png')}></Image> */}
                    </TouchableOpacity>
                </View>
            </Animated.View>
        )
    }
}