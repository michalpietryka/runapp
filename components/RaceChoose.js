import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Icon from './Icon.js'
import styles from '../styles';
import RunApi from '../modules/RunApi.js'

export default class RaceChoose extends React.Component {
    constructor(props) {
        super(props)

        this.api = new RunApi

        this.state = {
            races: []
        }

        this.renderElements = this.renderElements.bind(this)
    }

    componentWillMount() {
        this.api.getRaces().then((races) => {
            this.setState({
                races: races
            })
        })
    }

    getParsedDate(date){
        date = String(date).split(' ');
        var days = String(date[0]).split('-');
        var hours = String(date[1]).split(':');
        return [parseInt(days[0]), parseInt(days[1])-1, parseInt(days[2]), parseInt(hours[0]), parseInt(hours[1]), parseInt(hours[2])];
    } 

    renderElements() {
        return this.state.races.map(race => {
            let now = new Date()
            let start = new Date(...this.getParsedDate(race.start_date))
            now = parseInt(now / 1000)
            start = parseInt(start / 1000)
            race.timeToStart = start - now
            if(race.timeToStart < 5) return false
            
            let seconds = race.timeToStart % 60 >= 10 ? race.timeToStart % 60 : '0' + (race.timeToStart % 60)
            let minutes = (race.timeToStart - seconds) / 60 >= 10 ? (race.timeToStart - seconds) / 60 : '0' + ((race.timeToStart - seconds) / 60)

            return (
                <TouchableOpacity onPress={() => {this.props.startRace(race)}} key={race.id} style={styles.RaceListItem}>
                    <View style={{flexDirection: 'row', alignContent: 'center'}}>
                        <Icon style={StyleSheet.flatten([this.props.style, {fontSize: 32, color: '#FF4217'}])} icon={'flag'}></Icon>
                        <Text style={{marginTop: 8, fontSize: 10, color: '#B8B8B8'}}>Distance: </Text>
                        <Text style={{marginTop: 8, fontSize: 10}}>{race.track.distance}m</Text>
                        <Icon style={StyleSheet.flatten([this.props.style, {fontSize: 32, color: '#FF4217'}])} icon={'bullet'}></Icon>
                        <Text style={{marginTop: 8, fontSize: 10,  marginLeft: 16, marginRight: 16}}>{race.participants}</Text>
                        <Text style={{marginTop: 8, fontSize: 10, color: '#B8B8B8'}}>Time to start: </Text>
                        <Text style={{marginTop: 8, fontSize: 10}}>{minutes}:{seconds}
                        </Text>
                    </View>                    
                </TouchableOpacity>
            )
        })
    }

    render() {
        return (
            <View>
                {this.renderElements()}
            </View>
        )
    }
}