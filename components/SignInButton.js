import React from 'react';
import {
    Text,
    TouchableOpacity,
    StyleSheet 
} from 'react-native';
import { LinearGradient } from 'expo';
import styles from '../styles.js'

export default class SignInButton extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
		return (
		  <TouchableOpacity style={styles.SignInButton} delayLongPress={1000} onLongPress={this.props.onLongPress} onPress={this.props.onPress}>
                <LinearGradient start={[1, 0]} colors={['#FF4217', '#FFC466']} style={styles.SignInButtonGradient}>
			        <Text style={styles.ButtonText}>{this.props.text}</Text>
                </LinearGradient>
		  </TouchableOpacity>
		);
	}
} 