import { AsyncStorage } from "react-native"

const apiUrl = 'http://run.tmp.pl/';

export default class RunApi {
    async getPlayerCode() {
        AsyncStorage.clear()
        const value = await AsyncStorage.getItem('playerCode');
        if (value !== null) {
            return value;
        } else {
            let response = await fetch(apiUrl);
            let responseJson = await response.json();
            await AsyncStorage.setItem('playerCode', responseJson.code);
        }   
    }

    async updatePlayerPosition(playerCode, position) {
        await fetch(apiUrl, {
            method: 'POST',
            body: JSON.stringify({
                code: playerCode,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }),
        }).catch((error) => {
            console.log(error);
        });
    }

    async getTracks() {
        let response = await fetch(apiUrl + 'tracks').catch((error) => {
            console.log(error)
        });
        let responseJson = await response.json();
        return responseJson.tracks
    }

    async getRaces() {
        let response = await fetch(apiUrl + 'races').catch((error) => {
            console.log(error)
        });
        let responseJson = await response.json();
        return responseJson.races
    }

    async createNewRace(id) {
        let response = await fetch(apiUrl + 'dummy-race/'+id).catch((error) => {
            console.log(error)
        });
        return response
    }
}