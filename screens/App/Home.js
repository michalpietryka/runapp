import React from 'react';
import {
    View,
    Image,
    Text
} from 'react-native';
import { MapView, Constants, Location, Permissions } from 'expo';
import { Marker } from 'react-native-maps';

import TimerMixin from 'react-timer-mixin';
import Container from '../../components/Container.js'
import Legend from '../../components/Legend.js'
import BottomNav from '../../components/BottomNav.js'
import ContentCloseButton from '../../components/ContentCloseButton.js'
import RaceChoose from '../../components/RaceChoose.js'
import styles from '../../styles.js';
import RunApi from '../../modules/RunApi.js'

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)

        this.api = new RunApi

        this.state = {
            location: null,
            errorMessage: null,
            playerCode: null,
            // tracks: [],

            contentShow: false,

            currentRace: null,
            currentCheckpoint: null, // zaliczony checkpoint

            raceStart: null,
            time: null
        }

        this.renderMap = this.renderMap.bind(this)
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
        this.startRace = this.startRace.bind(this)
        this.setNextCheckpoint = this.setNextCheckpoint.bind(this)

        setInterval(() => {
            if(this.state.currentRace && this.state.raceStart) {
                let newTime = new Date() - this.state.raceStart
                if(!isNaN(newTime)) {
                    this.setState({
                        time: new Date() - this.state.raceStart
                    })
                }
            }
        }, 100)
    }

    componentWillMount() {
        this._getLocationAsync();
        this.api.getPlayerCode().then((result) => {
            this.setState({
                playerCode: result
            }, () => {
                this.timer = setInterval(() => {
                    this._getLocationAsync().then(() => {
                        this.api.updatePlayerPosition(this.state.playerCode, this.state.location)
                        this.updateRace()
                    })
                }, 3000)
            })
        })
    }

    updateRace() {
        if(!this.state.currentRace) return false 

        let now = new Date()
        let start = new Date(...this.getParsedDate(this.state.currentRace.start_date)) 
        now = parseInt(now / 1000)
        start = parseInt(start / 1000)

        if(now < start) return false //jeszce nie wystartował

        let ckp = this.state.currentRace.track.checkpoints
        let toCheck = null
        if(!this.state.currentCheckpoint) toCheck = ckp[0]
        if(this.state.currentCheckpoint) {
            for(var i = 0; i < ckp.length; i++) {
                if(this.state.currentCheckpoint.id >= ckp[i].id) continue
                toCheck = ckp[i]
                break
            }
        }

        /** Sprawdzenie, czy należy zaliczyć checkpoint (liczenie odległości między dwoma punktami) */
        let x1 = this.state.location.coords.latitude
        let y1 = this.state.location.coords.longitude
        let x2 = toCheck.latitude
        let y2 = toCheck.longitude
        let distance = this.getDistance(x1, y1, x2, y2)
        if(distance < 10) this.setNextCheckpoint()
    }

    getDistance(x1, y1, x2, y2) {
        let distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow( Math.cos(x1*3.14/180) * (y2 - y1), 2)) * (40075.704/360) * 1000
        return distance
    }

    setNextCheckpoint() {
        let ckp = this.state.currentRace.track.checkpoints
        if(!this.state.currentCheckpoint) {
            this.setState({
                currentCheckpoint: ckp[0],
                raceStart: new Date()
            })
            return true
        }
        for(var i = 0; i < ckp.length; i++) {
            if(ckp[i].id <= this.state.currentCheckpoint.id) continue
            this.setState({
                currentCheckpoint: ckp[i]
            })
            if(i + 1 < ckp.length) return true;
        }
        /** Nie ma kolejnych - zakończenie */
        console.log('END')
        this.setState({
            currentRace: null,
            currentCheckpoint: null
        })
    }

    async _getLocationAsync() {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }
    
        let location = await Location.getCurrentPositionAsync({});
        this.setState({ location });
    };

    startRace(race) {
        this.setState({
            currentRace: race,
            contentShow: false
        })
    }

    getParsedDate(date){
        date = String(date).split(' ');
        var days = String(date[0]).split('-');
        var hours = String(date[1]).split(':');
        return [parseInt(days[0]), parseInt(days[1])-1, parseInt(days[2]), parseInt(hours[0]), parseInt(hours[1]), parseInt(hours[2])];
    } 

    renderCheckpoints() {
        if(!this.state.currentRace) return false
        let icon = require('../../assets/img/flag.png')
        let title = 'Checkpoint'
        var i = 0
        var next = false

        return this.state.currentRace.track.checkpoints.map(checkpoint => {
            if(this.state.currentCheckpoint && checkpoint.id <= this.state.currentCheckpoint.id) {
                icon = require('../../assets/img/flag_checked.png')
                title += ' checked'
            } else if((!this.state.currentCheckpoint && i == 0) || !next) {
                icon = require('../../assets/img/flag_next.png')
                title += ' next'
                next = true
            } else {
                icon = require('../../assets/img/flag.png')
                title = 'Checkpoint'
            }
            i++

            return (
                <Marker key={checkpoint.id}
                    coordinate={{
                        latitude: parseFloat(checkpoint.latitude),
                        longitude: parseFloat(checkpoint.longitude)
                    }}
                    title={title}
                    image={icon}
                />
            )
        })
    }

    renderMap() {
        if(this.state.location) {
            return (
                <MapView
                    style={{ flex: 1 }}
                    zoom={20}
                    initialRegion={{
                        latitude: this.state.location.coords.latitude,
                        longitude: this.state.location.coords.longitude,
                        latitudeDelta: 0.0220,
                        longitudeDelta: 0.0100,
                    }}
                >
                    <Marker
                        coordinate={{
                            latitude: this.state.location.coords.latitude,
                            longitude: this.state.location.coords.longitude
                        }}
                        title={'You'}
                        image={require('../../assets/img/you.png')}
                    />
                    {this.renderCheckpoints()}
                </MapView>
            )
        }
    }

    renderContent() {
        if(!this.state.contentShow) return false
        if(this.state.currentRace || this.state.time) {
            let miliseconds = this.state.time % 1000
            let seconds = (this.state.time - miliseconds) / 1000

            return (
                <View style={styles.ContentBox}>
                    <Text>{seconds}.{miliseconds}</Text>
                </View>
            )
        } else {
            return(
                <View style={styles.ContentBox}>
                    <RaceChoose startRace={this.startRace}></RaceChoose>
                    <ContentCloseButton icon={'close'}></ContentCloseButton>
                </View>
            )
        }
    }
    
    render() {
        return (
            <Container style={{position: 'relative'}}>
                <Legend></Legend>
                <BottomNav dummyButton={() => { this.setNextCheckpoint() }} createDummyRace={() => { this.api.createNewRace(1); this.setState({time:null}) }} openContent={() => {this.setState({contentShow: !this.state.contentShow})}}></BottomNav>
                {this.renderMap()}
                {this.renderContent()}
            </Container>
        )
    }
}