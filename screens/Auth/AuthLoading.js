import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
} from 'react-native';

export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    isTokenValid(token) {
        let expireDate = new Date(token.expires_in)
        if(new Date().getTime() < expireDate.getTime()) return true
        return false
    }

    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        if(true || userToken && this.isTokenValid(JSON.parse(userToken))) {
            this.props.navigation.navigate('App')
        } else {
            this.props.navigation.navigate('Auth')
        }
    } 

    render() {
        return (
            <React.Fragment>
                <StatusBar barStyle="default" />
                <ActivityIndicator />                
            </React.Fragment>
        )
    }
}