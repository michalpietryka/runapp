import React from 'react';
import {
    AsyncStorage,
    Button,
    Image,
    View,
    StyleSheet,
    Text,
    TextInput
} from 'react-native';
import Container from '../../components/Container.js'
import SignInButton from '../../components/SignInButton.js'
import styles from '../../styles.js'
import axios from 'axios'

export default class SignInScreen extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            login: '',
            password: '',
            errors: [],
            token: null,
            isLoading: false
        }
    }

    _renderErrors() {
        if(this.state.errors.length > 0) {
            return (
                <View style={styles.ErrorsGroup}>
                    <Text style={styles.Error}>{this.state.errors[0]}</Text>
                </View>
            )
        }
    }

    _setToken = async(data) => {
        try {
            await AsyncStorage.setItem('userToken', JSON.stringify(this.state.token));
            this.props.navigation.navigate('App');
        } catch(error) {
            console.log(error)
        }
    }

    _signInAsync = async () => {
        let errors = [];
        if(!this.state.login) errors.push("'Login' field must not be empty.")
        if(!this.state.password) errors.push("'Password' field must not be empty.")
        this.setState({errors: errors})
        
        if(errors.length === 0) {
            this.setState({isLoading: true})

            axios.post('http://run.tmp.pl/oauth/token', {
                'grant_type': 'password',
                'client_id': 2,
                'client_secret': 'qVQY3mTfC4m9mo3PtL9qK4WWWVvoEaFJf3saUnzU',
                'scope': '',
                'username': this.state.login,
                'password': this.state.password,
            })
                .then((response) => {
                    this.setState({
                        token: {
                            access_token: response.data.access_token,
                            refresh_token: response.data.refresh_token,
                            expires_in: new Date().getTime() + response.data.expires_in * 1000,
                            token_type: response.data.token_type
                        }
                    })
                    this._setToken()

                    this.setState({isLoading:false})
                })
                .catch((error) => {
                    alert('Wystąpił błąd w trakcie logowania. Spróbuj ponownie lub skontaktuj się z twórcami aplikacji.')
                    this.setState({isLoading:false})
                })
        }
    };

    render() {
        return (
            <Container isLoading={this.state.isLoading}  navigation={this.props.navigation}>
                <View style={stl.container}>
                    <Image style={{width: 130, height: 130, alignSelf:'center'}}source={require('../../assets/img/logo.png')}></Image>
                    <Text style={styles.TextLogo}>runapp</Text>

                    <View style={styles.InputGroup}>
                        <TextInput 
                            value={this.state.first_name} 
                            onChangeText={(text) => this.setState({login: text})} 
                            placeholder={'Login'}
                            placeholderTextColor={'#B8B8B8'}
                            maxLength={50} 
                            underlineColorAndroid="transparent" 
                            style={styles.TextInput} />
                    </View>

                    <View style={styles.InputGroup}>
                        <TextInput 
                            secureTextEntry={true} 
                            value={this.state.second_name} 
                            onChangeText={(text) => this.setState({password: text})} 
                            placeholder={'Password'}
                            placeholderTextColor={'#B8B8B8'}
                            maxLength={50} 
                            underlineColorAndroid="transparent" 
                            style={styles.TextInput} />
                    </View>
                    
                    <View style={stl.btnContainer}>
                        <SignInButton text="Sign in"onPress={this._signInAsync} />
                    </View>

                    {this._renderErrors()}
                </View>
            </Container>
        )
    }
}

const stl = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 80,
        paddingLeft: 80,
        paddingRight: 80
    },
})