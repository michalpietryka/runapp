import {
    StyleSheet,
    PixelRatio
} from 'react-native';

const cl_white = '#ffffff';
const cl_black = '#333333';
const cl_grey = '#B8B8B8';
const cl_grey_light = '#E5E5E5';
const cl_primary = '#FF4217';
const cl_secondary = '#FFC466';

const color_pink = '#ffffff';

const closeButtonSize = 12

export default styles = StyleSheet.create({
    clPrimary: {color:cl_primary},
    clGreyLight: {color:cl_grey_light},
    clGrey: {color:cl_grey},
    clBlack: {color:cl_black},
    fontSemiBold: {fontFamily: 'Nunito-SemiBold'},

    ContentBox: {
        backgroundColor: '#ffffff',
        position: 'absolute',
        bottom: 150,
        width: '86%',
        left: '7%',
        zIndex: 10,
        elevation: 15,
        padding: 16,
        borderRadius: 10,
        paddingBottom: 32,
        paddingTop: 32
    },

    ContentCloseButton: {
        width: closeButtonSize * 2,
        height: closeButtonSize * 2,
        position: "absolute",
        top: closeButtonSize * -1 - 8,
        right: closeButtonSize * -1
    },  

    RaceListItem: {
        borderColor: '#E5E5E5',
        borderWidth: 2,
        borderRadius: 10,
        paddingTop: 12,
        paddingBottom: 12,
        marginBottom: 6
    },

    TextInput: {
        backgroundColor: '#ffffff',
        borderColor: cl_grey_light,
        borderWidth: 1.5,
        borderRadius: 10,
        width: '100%',
        
        fontSize: 14,
        color: cl_black,
        textAlign: 'center',
        fontFamily: 'Nunito-SemiBold',

        minWidth: 200,
        paddingTop: 8,
        paddingBottom: 8,
    },

    TextLogo: {
        color: cl_black,
        fontSize: 20,
        textAlign: 'center',
        position: 'relative',
        top: -20,
        marginBottom: 16
    },

    SignInButtonGradient: {
        borderRadius: 10,
        minWidth: 200,
        paddingTop: 12,
        paddingBottom: 12,
    },
    
    ButtonText: {
        fontSize: 14,
        color: cl_white,
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
    },

    SignInButton: {
    },

    InputGroup: {
        marginBottom: 14
    },

    ErrorsGroup: {
        marginTop: 14
    },

    Error: {
        color: cl_primary,
        fontSize: 12,
        borderRadius: 10,
    },

    LegendContainer: {
        position: 'absolute',
        zIndex: 10,
        top: 0,
        left: 0,
        flex: 1,
        flexDirection: 'row'
    },

    LegendOffcanvas: {
        backgroundColor: cl_white,
        borderBottomRightRadius: 10,
        paddingBottom: 0,
        paddingTop: 24,
        paddingLeft: 24,
        paddingRight: 24
    },

    ToggleLegendButton: {    
        width: 40,
        height: 40,
        paddingTop: 10,
        backgroundColor: cl_white,
        borderBottomRightRadius: 10
    },

    LegendIconContainer: {
        flex: 1,
        alignSelf: 'center'
    },

    Icon: {
        fontFamily: 'Icons',
        textAlign: 'center'
    },

    BottomNavContainer: {
        backgroundColor: cl_white,
        height: 56,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        zIndex: 100,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        elevation: 10,
        paddingTop: 10,
        flex:1,
        flexDirection: 'row',
    }
});